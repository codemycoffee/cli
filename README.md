# React CLI

Node based command line application that allows the user to create react components with specific name conventions and predefined templates.

## Installation

Global installation to call `rcli` from any place in the terminal

`npm install -g ./`

## Commands

* **tree|t** -> Interactive folder tree structure generation

* **component|c** *[options]* *<name>* *[subcomponent...]* ->
Generate a new component with default css and test files

* **dock|dk** *[options]* *[container]* ->   
Trigger several docker and docker-compose common commands or set of commands

### Tree
Several interactive promts would be provided to generate as many components as desired in a tree folder structure.

```
? ()Create component? Yes
? Name (in snake_case): admin_page
? Type of component Stateful
? (admin_page)Create child?: Yes
? Name (in snake_case): table
? Type of component (Press <space> to select, <a> to toggle all, <i> to invert selection)
❯◯ Stateful
 ◯ Stateless
```

The first letters between `()` refers to the current path

`? (admin_page/table/table_row)Create child?: (Y/n)`

### Component

This command accepts 3 different kind of input

**Single component**: 

Input: `rcli admin` 
Result:
```
components
|-admin
|-|-Admin.js
|-|-Admin.css
|-|-Admin.test.js
```

**Multiple sibling components**

Input: `rcli admin user menu`
Result:
```
components
|-admin
|-|-Admin.js
|-|-Admin.css
|-|-Admin.test.js
|-user
|-|-User.js
|-|-User.css
|-|-User.test.js
|-menu
|-|-Menu.js
|-|-Menu.css
|-|-Menu.test.js
```
**Child components**

Input: `rcli admin/table/table_row`
Output: 
```
components
|-admin
|-|-Admin.js
|-|-Admin.css
|-|-Admin.test.js
|-|table
|-|-|-Table.js
|-|-|-Table.css
|-|-|-Table.test.js
|-|-|-table_row
|-|-|-|-TableRow.js
|-|-|-|-TableRow.css
|-|-|-|-TableRow.test.js
```


