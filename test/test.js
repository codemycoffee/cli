var assert = require('assert')
var chai = require('chai')
chai.use(require('chai-fs'))
var assert = chai.assert
var mock = require('mock-fs')
var path = require('path')
var fileSystemHelpers = require('../src/utils/fileSystemHelpers')
var component = require('../src/commands/component')
var formatter = require('../src/utils/formatter')
var fill = require('../src/utils/fillTemplate')

describe("Formatter", function(){
    describe('#componentRename()', function(){
        it('Should return a PascalCase string from snake case format', function(){
            // Single word
            assert.equal(formatter.componentRename('some'), 'Some')
            // Proper short snake case
            assert.equal(formatter.componentRename('some_name'), 'SomeName')
            // Proper long snake case
            assert.equal(formatter.componentRename('some_name_longer'), 'SomeNameLonger')
            // TODO: Wrong inputs test
        });
    });
});

describe("Fill Template", function(){
    describe('#replaceKeyword()', function(){
        // TODO: test with different keywords and maybe some symbols
        it('Should replace a keyword in the template for another', function(){
            mock({
                'root':{
                    'components':{
                        'main':{
                            'somecomponent':'Bacon ipsum dolor name amet'
                        }
                    }
                }
            })

            fill.replaceKeyword('name', 'NewKeyword', 'root/components/main/somecomponent', function(res){
                assert.fileContent('root/components/main/somecomponent','Bacon ipsum dolor NewKeyword amet')    
            })
        })
    })
})

describe("Generators",function(){
    describe('#generateFolder()',function () {
        it('Should create a folder if it does not exist',function () {
            mock({
                'rootpath':{}
            });
            // Create a single folder and return the created path
            var newFolder = fileSystemHelpers.generateFolder(path.join('rootpath','components'))
            assert.pathExists('rootpath/components')
            assert.equal(newFolder,'rootpath/components')
            // Multiple level folder creation
            var newFolder = fileSystemHelpers.generateFolder(path.join('rootpath','components','component_name'))
            assert.pathExists('rootpath/components/component_name')
            assert.equal(newFolder,'rootpath/components/component_name')

            mock.restore()
        })
    })
    describe('#generateFile()',function(){
        it('Should create a file in a specific path with a determined name and file extension', function(){
            mock({
                'root':{
                    'components':{
                        'main':{}
                    }
                }
            })
            // Ideal case, all parameters are correct
            var file = fileSystemHelpers.generateFile('RandomComponent','root/components/main','js')
            fileSystemHelpers.generateFile('RandomComponent','root/components/main','css')
            fileSystemHelpers.generateFile('RandomComponent','root/components/main','test.js')

            assert.directoryFiles('root/components/main', ['RandomComponent.js','RandomComponent.css','RandomComponent.test.js'])
            assert.isEmptyFile('root/components/main/RandomComponent.js')
            assert.isEmptyFile('root/components/main/RandomComponent.css')
            assert.isEmptyFile('root/components/main/RandomComponent.test.js')

            assert.strictEqual(file, 1)
            // Wrong path provided
            file = fileSystemHelpers.generateFile('RandomComponent','root/fail/main','js')
            assert.equal(file.code, 'ENOENT')

            // TODO: Test invalid symbols in extension path and names

            mock.restore()
        })
    })

    describe('#generateFromTemplate()',function(){
        it('Should create a file in a specific path with a determined name and file extension from a predefined template', function(){
            mock({
                'root':{
                    'components':{
                        'main':{}
                    },
                    'templates':{
                        'mainTemplate':'Bacon ipsum dolor name amet'
                    }
                }
            })
            // Ideal case, all parameters are correct
            fileSystemHelpers.generateFileFromTemplate('RandomComponent','root/components/main','mainTemplate', 'root', function(err){
                assert.fileContent('root/components/main/RandomComponent.js','Bacon ipsum dolor RandomComponent amet')
                assert.directoryFiles('root/components/main', ['RandomComponent.js'])
                assert.notIsEmptyFile('root/components/main/RandomComponent.js')

                mock.restore() 
            })
            
            // TODO: Test invalid symbols in extension path and names
        })
    })

    describe('#generateComponent()',function(){
        it('Should generate a single component', function(){
            mock({
                'root':{
                    'components':{
                        'main':{}
                    },
                    'templates':{
                        'stateComponent':'Bacon ipsum dolor name amet'
                    }
                }
            })
            component.generateComponents('admin', 'root', false, [], function(){
                assert.pathExists('root/components/admin')
                assert.directoryFiles('root/components/admin',['Admin.js','Admin.css','Admin.test.js'])

                mock.restore() 
            })
        })

        it('Should generate multiple sibling components', function(){
            mock({
                'root':{
                    'components':{
                        'main':{}
                    },
                    'templates':{
                        'stateComponent':'Bacon ipsum dolor name amet'
                    }
                }
            })
            component.generateComponents('admin', 'root', false, ['admin_page','user_page'], function(){
                assert.pathExists('root/components/admin')
                assert.pathExists('root/components/admin_page')
                assert.pathExists('root/components/user_page')

                assert.directoryFiles('root/components/admin',['Admin.js','Admin.css','Admin.test.js'])
                assert.directoryFiles('root/components/admin_page',['AdminPage.js','AdminPage.css','AdminPage.test.js'])
                assert.directoryFiles('root/components/user_page',['UserPage.js','UserPage.css','UserPage.test.js'])

                mock.restore() 
            })
        })

        it('Should generate a single component with undetermined child components', function(){
            mock({
                'root':{
                    'components':{
                        'main':{}
                    },
                    'templates':{
                        'stateComponent':'Bacon ipsum dolor name amet'
                    }
                }
            })
            component.generateComponents('admin/table/table_row', 'root', false, [], function(){
                assert.pathExists('root/components/admin')
                assert.pathExists('root/components/admin/table')
                assert.pathExists('root/components/admin/table/table_row')

                assert.directoryFiles('root/components/admin',['Admin.js','Admin.css','Admin.test.js'])
                assert.directoryFiles('root/components/admin/table',['Table.js','Table.css','Table.test.js'])
                assert.directoryFiles('root/components/admin/table/table_row',['TableRow.js','TableRow.css','TableRow.test.js'])

                mock.restore()   
            })
        })
    })

})