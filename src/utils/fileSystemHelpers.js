var fs = require('fs')
var path = require('path');
var fillTemplate = require('./fillTemplate')


/**
 * Checks if a folder exists in a especific path and creates it in case it's non existent yet
 * @param {Path} path Path where the folder would be created
 * 
 * @returns {path}
 * @returns {boolean}
 */
function generateFolder(path) {
    if(!fs.existsSync(path)){
        fs.mkdirSync(path)
    }
    return path
}

/**
 * Create a file with defined name path and extension
 * @param {String} name Name of the file (same name as the component)
 * @param {String} filePath Path where the file is going to be generated
 * @param {String} extension Extension of the file (js/css/test.js)
 */
function generateFile(name, filePath, extension) {
    //TODO: check if component folder and/or file exists
    var finalPath = path.join(filePath,`${name}.${extension}`)
    
    try {
        var file = fs.openSync(finalPath, 'w')
        fs.closeSync(file)
        return file
    } catch (error) {
        return error
    }
}

/**
 * Makes a copy of a template and substitutes some keywords in the template for actual parameters
 * @param {String} name Name of the component
 * @param {String} path Path where the file is going to be copied and edited
 * @param {String} template Name of the reference template
 */
// TODO: make it for any kind of template extension and keyword replacement. ONLY WORKS FOR COMPONENT TEMPLATES CURRENTLY
function generateFileFromTemplate(name, filePath, templateName, rootPath){
    var templatePath = path.join(rootPath,'templates',templateName)
    var componentFilePath = path.join(filePath,`${name}.js`)
    fs.copyFileSync(templatePath,componentFilePath)
    fillTemplate.replaceKeyword('name',name,componentFilePath)
}



var exports = module.exports = {
    generateFolder,
    generateFile,
    generateFileFromTemplate
}