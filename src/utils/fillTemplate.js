var fs = require('fs')

/**
 * 
 * @param {String} keyword String in the template to be replaced
 * @param {String} replacement Word that will replace the keyword
 * @param {Path} file Path to the file to be edited *WARNING* DON'T REPLACE THE TEMPLATE FILE
 */
function replaceKeyword(keyword,replacement,file){
    fs.readFile(file, 'utf8', function (err,data) {
        if (err) {
          return console.log(err);
        }
        var keyExpresion = new RegExp(keyword, "g")
        var result = data.replace(keyExpresion, replacement);
      
        fs.writeFile(file, result, 'utf8', function (err) {
           if (err) return console.log(err);
        });
      });
}

var exports = module.exports = {
    replaceKeyword
}