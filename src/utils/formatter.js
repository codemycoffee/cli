/**
 * Converts to PascalCase from snake_case
 * @param {String} name Snake case string to be formatted
 */
function componentRename(name) {
    // TODO: make this a general any string to pascal case
    var splitted = name.split("_")
    for(word in splitted){
        splitted[word] = splitted[word].replace(/^\w/, c => c.toUpperCase())
    }
    return splitted.join("");
}

var exports = module.exports = {
    componentRename
}