#!/usr/bin/env node

var program = require('commander');
var component = require("./commands/component");
var dock = require("./commands/docker");
var tree = require('./commands/tree')

program.version('0.0.1')

program
    .command('tree')
    .alias('t')
    .description('Interactive folder tree structure generation')
    .action(function(){
        tree.generateTree(__dirname, '')
    })

program
    .command('component <name> [subcomponent...]')
    .alias('c')
    .description('Generate a new component with default css and test')
    .option('-s, --stateless', 'Select type of component')
    .action(function(name,subcomponent, options){
        component.generateComponents(name,__dirname, options.stateless, subcomponent)
    })

program
    .command('dock [container]')
    .alias('dk')
    .description('Trigger several docker and docker-compose common commands or set of commands')
    .option('-r, --run', "Build and run a docker-compose file. Display docker processes after")
    .option('-s, --stop', "Stop all containers")
    .option('-d, --delete', "Stop and erase volumes")
    .option('-re, --restart', "Stop containers, and re run them again")
    .option('-l, --log', "Show the logs for a specific container")
    .option('-b, --bash', "Attach terminal to container")
    .action(function(container, options){
        if(options.run){
            dock.run()
        }
        if(options.stop){
            dock.stopContainer()
        }
        if(options.delete){
            dock.del()
        }
        if(options.restart){
            dock.restart()
        }
        if(options.log && container !== undefined){
            dock.logContainer(container)
        }
        if(options.bash && container !== undefined){
            dock.attach(container)
        }
        console.log("Must specify a flag. There is no defult command to be executed")
    })

program.parse(process.argv);