var inquirer = require('inquirer')
var path = require('path')
var component = require('./component')
var colors = require('colors')

/**
 * *DOCUMENT THIS SHIT ASAP
 * @param {*} rootPath 
 * @param {*} parent 
 */
function generateTree(rootPath, parent){
    inquirer.prompt([
        {
            type:'confirm',
            name:'generateComponent',
            message:colors.green('('+parent+')')+'Create component?'
        }
    ]).then( answers => {
        if(answers.generateComponent){
            newComponent(rootPath, parent)
        }else if(parent.indexOf("/") != -1){
            var route = parent.split('/');
            route.pop();
            generateTree(rootPath, route.join('/'))
        }else if(parent == ''){
            return
        }else{
            generateTree(rootPath,'')
        }
    })
}

function newComponent(root,parent){
    var name = ''
    var isStateless = false
    inquirer.prompt([
        {
            type:'input',
            name:'name',
            message:'Name (in snake_case):'
        }
    ]).then( answers => {
        name = parent.length > 1 ? path.join(parent,answers.name) : answers.name

        inquirer.prompt([
            {
                type:'checkbox',
                name:'isStateless',
                message:'Type of component',
                choices: ['Stateful', 'Stateless']
            }
        ]).then( answers => {
            isStateless = answers.isStateless
            inquirer.prompt([
                {
                    type:'confirm',
                    name:'child',
                    message:colors.green('('+name+')')+'Create child?:'
                }
            ]).then(answers => {
                if(answers.child){
                    newComponent(root, name)
                }else{
                    // TODO: fix estateless reference generation
                    component.generateComponents(name,root,answers.isStateless, [])
                    generateTree(root,parent)
                }
            })
        })
    })

}

var exports = module.exports = {
    generateTree
}