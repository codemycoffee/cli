var settings = require('../settings.json');
var formatter = require('../utils/formatter');
var path = require('path');
var fileSystemHelpers = require('../utils/fileSystemHelpers')
/**
 * @param {String} name Component name in snake_case 
 * @param {String} rootPath Root directory for the app
 * @param {Boolean} isStateless Defines the template for the main component
 * @param {String[]} extraComponents Array containing the name of the extra components
 */
function generateComponents(name, rootPath, isStateless, extraComponents) {
    fileSystemHelpers.generateFolder(path.join(rootPath, 'components'))
    extraComponents.push(name)

    for(component in extraComponents){
        var folderName = extraComponents[component]
        var componentName = formatter.componentRename(folderName)
        var parentPath = path.join(rootPath,'components')

        if(name.indexOf("/") != -1){
            var nestedComponents = name.split("/")
            
            for(nested in nestedComponents){
                var componentPath = fileSystemHelpers.generateFolder(path.join(parentPath,nestedComponents[nested]))
                if(!componentPath){
                    
                }else{
                    componentName = formatter.componentRename(nestedComponents[nested])
                    createComponent(componentName, componentPath, isStateless, rootPath)
                    parentPath = componentPath
                }
            }
        }else{
            var componentPath = fileSystemHelpers.generateFolder(path.join(parentPath,extraComponents[component]))
            componentName = formatter.componentRename(extraComponents[component])
            createComponent(componentName, componentPath, isStateless, rootPath)
        }
    }
}

function createComponent(name, directoryPath, isStateless, rootPath){
    var extensions = settings.component.file_extensions
    var template = isStateless ? settings.component.templates.stateless : settings.component.templates.state

    for (extension in extensions){
        if (extensions[extension] === "js") {
            fileSystemHelpers.generateFileFromTemplate(name, directoryPath, template, rootPath)
        } else {
            fileSystemHelpers.generateFile(name, directoryPath, extensions[extension])   
        }
    }
}

var exports = module.exports = {
    createComponent,
    generateComponents
}