var build = 'docker-compose build'
var up = 'docker-compose up -d'
var down = 'docker-compose down'
var stop = 'docker-compose down -v'
var log = 'docker-compose logs container'
var exec = 'docker exec -it container bash'
var ps = 'docker ps -a'

var cmd = require('node-cmd');

function run(){
    cmd.run(build)
    cmd.run(up)
    cmd.run(ps)
}

function stopContainer(){
    cmd.run(down)
}

function del(){
    cmd.run(stop)
}

function restart(){
    cmd.run(down)
    cmd.run(up)
    cmd.run(ps)
}

function logContainer(container){
    newLog = log.replace("container",container)
    cmd.run(newLog)
}

function attach(container){
    newBash = exec.replace('container', container)
    cmd.run(exec)
}

var exports = module.exports = {
    run,
    stopContainer,
    del,
    restart,
    logContainer,
    attach
}